<?php
require_once __DIR__ . '/../../boot.php';

$user_count = (int) $argv[1];

//$command = $app->db->createCommand('SELECT * FROM user WHERE id > 2');
//$command->execute();
//while($user = $command->pdoStatement->fetch(\PDO::FETCH_ASSOC)) {
for($j = 0; $j < $user_count; $j++) {
    $user = require 'user.php';
    $parent = selectSingle('SELECT id FROM user WHERE id > 1 ORDER BY RAND() LIMIT 1');
    if ($parent) {
        $user['parent_id'] = $parent->id;
    }
    insert('user', $user);
    $user['id'] = selectSingle('SELECT id FROM user WHERE username = :username', [
        ':username' => $user['username']
    ])->id;
    $ip = rand(16777216, 3758096384);
    $invoices = rand(1, 5);

    for($k = 0; $k < $invoices; $k++) {
        $time = time();
        $time = rand($time - 3600 * 24 * 50, $time);
        insert('invoice', [
            'status' => 'SUCCESS',
            'amount' => rand(1, 5) * 10,
            'sender_id' => $user['id'],
            'receiver_id' => 1,
            'payer' => $user['perfect'],
            'payee' => $app->perfect->wallet,
            'created_at' => $time,
            'updated_at' => $time + rand(30, 600),
            'ip' => $ip
        ]);
    }
    echo $user['username'] . " " . $invoices . "\n";
}
