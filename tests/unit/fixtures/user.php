<?php
use common\models\User;

$stake = rand(5, 20) * 5;
$first = $faker->firstName;
$last = $faker->lastName;
$name = strtolower($first) . '_' . strtolower($last);
$name = str_replace("'", '', $name);
$perfect = ['U'];
for($i = 0; $i < 7; $i++) {
    $perfect[] = rand(0, 9);
}
$perfect = implode('', $perfect);

return [
    'username' => $name,
    'email' => $name . '@yopmail.com',
    'status' => User::STATUS_FAKE,
    'first' => $first,
    'last' => $last,
    'phone' => rand(123456789, 12345678901234),
    'skype' => $name,
    'perfect' => $perfect,
    'password_hash' => Yii::$app->getSecurity()->generatePasswordHash('1234567kipr'),
    'auth_key' => Yii::$app->getSecurity()->generateRandomString(),
];
