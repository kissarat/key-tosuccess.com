<?php
$stake = rand(5, 20) * 5;
return [
    'name' => $faker->name,
    'degree' => 2,
    'priority' => rand(0, 10),
    'stake' => $stake,
    'fee' => $stake / 5,
];
