<?php
/**
 * Created by PhpStorm.
 * User: taras
 * Date: 6/8/15
 * Time: 12:13 AM
 */

namespace tests\unit\fixtures;


use yii\test\ActiveFixture;

class UserFixture extends ActiveFixture {
    public $modelClass = 'common\models\User';
}