CREATE TEMPORARY TABLE s LIKE user;

INSERT INTO s VALUES
(1,'admin','UUn-WjAzDIPF0OscCJObS03ZRGL8iJfq','$2y$13$1us22uXYP0uqND8Evw0hrO4ZMt2eu98BRGMC/hHH0UlIqGKoAigZa',NULL,'kissarat@gmail.com',10,1434028293,1434225382,'U1712339',5.00,'Тарас','Лабяк','380671541943','kissarat',NULL),
(2,'kissarat','A0PVZXPQiHwJCSbtCoGQns6BcZELopw_','$2y$13$N3x.4YYU9Fd3TdP5FCebpe5bHmrmlCG.MYqgcrIw3pgMewozuewn.',NULL,'lab_tas@ukr.net',10,1434028386,1434028386,'U9155945',0.00,'Андрей','Лабяк','380938320139','taradox89',1),
(3,'taras','tRsEG_L0pJfin9fClxGC4pSe1ngWfS79','$2y$13$FpEa.nMXMOdLaOIOSC1wjeiFbHBST.ARZ2/0X9m/81/aXKwt/NoKC',NULL,'taras@yopmail.com',10,1434028486,1434225382,'U9525021',25.00,'Taras','Томкив','380678888888','tarassik',1);

update user join s on user.id = s.id set
  user.username = s.username,
  user.email = s.email,
  user.parent_id = s.parent_id,
  user.perfect = s.perfect,
  user.phone = s.phone,
  user.first = s.first,
  user.last = s.last,
  user.skype = s.skype,
  user.account = s.account;
