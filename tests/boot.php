<?php
defined('YII_DEBUG') or define('YII_DEBUG', true);
defined('YII_ENV') or define('YII_ENV', 'dev');
define('ROOT', __DIR__ . '/..');

require_once ROOT . '/vendor/autoload.php';
require_once ROOT . '/vendor/yiisoft/yii2/Yii.php';
require_once ROOT . '/common/config/bootstrap.php';

use Faker\Factory;
use yii\console\Application;

$config = array_merge(
    require ROOT . '/console/config/main.php',
    require ROOT . '/console/config/main-local.php',
    require ROOT . '/common/config/main.php',
    require ROOT . '/common/config/main-local.php'
);

$app = new Application($config);
$app->init();
Yii::$app = $app;
$faker = Factory::create();

function insert($table, array $columns) {
    global $app;
    return $app->db->createCommand()->insert($table, $columns)->execute();
}

function selectSingle($sql, array $params = null, $class = "stdClass") {
    global $app;
    $command = $app->db->createCommand($sql, $params);
    $command->execute();
    return $command->pdoStatement->fetchObject($class);
}

function parametrize(array $array) {
    $result = [];
    foreach($array as $key => $value) {
        $result[':' . $key] = $value;
    }
    return $array;
}
