<?php
require_once __DIR__ . '/../boot.php';
require_once 'Form.php';

use common\models\User;
use tests\acceptance\Form;

function open_matrix($username)
{
    $form = new Form('/site/login', 'LoginForm');
    $form->fill([
        'username' => $username,
        'password' => '1234567kipr',
    ]);
    $form->send();

    $form->go('/tree/open');
    $raw = $form->send();
//    file_put_contents("/home/taras/www/yii/frontend/web/out/$username.html", $raw);
//    $form->go('/site/logout');
}

$users = User::find()->select('username')->where(['>', 'id', 1])->orderBy(['id' => SORT_ASC])->all();

foreach($users as $user) {
    echo "# User: $user->username\n";
    open_matrix($user->username);
}
