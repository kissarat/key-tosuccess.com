<?php

/*

This script demonstrates transfer proccess between two
PerfectMoney accounts using PerfectMoney API interface.

*/

// trying to open URL to process PerfectMoney Spend request
$params = [
    'AccountID' => '3442748',
    'PassPhrase' => 'tX*82yU#1+',
    'Payer_Account' => 'U1712339',
    'Payee_Account' => 'U9155945',
    'Amount' => '0.01',
    'PAY_IN' => '1',
    'PAYMENT_ID' => '-1',
];

$f=fopen('https://perfectmoney.is/acct/confirm.asp?' . http_build_query($params), 'rb');

if(false === $f) {
    echo 'error openning url';
}

// getting data
$out=array();
while(!feof($f)) {
    $out[]= fgets($f);
}

fclose($f);
$out = implode('', $out);

// searching for hidden fields
if(!preg_match_all("/<input name='(.*)' type='hidden' value='(.*)'>/", $out, $result, PREG_SET_ORDER)){
    echo 'Ivalid output';
    exit;
}

$ar="";
foreach($result as $item){
    $key=$item[1];
    $ar[$key]=$item[2];
}

echo '<pre>';
print_r($ar);
echo '</pre>';