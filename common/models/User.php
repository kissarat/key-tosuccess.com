<?php
namespace common\models;

use frontend\Entity;
use frontend\models\Tree;
use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\web\IdentityInterface;

/**
 * User model
 *
 * @property integer $id
 * @property string $username
 * @property string $password write-only password
 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $auth_key
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $email
 * @property string $first
 * @property string $last
 * @property integer $phone
 * @property string $skype
 * @property string $perfect
 * @property number account
 * @property int qualification
 * @property int $parent_id
 * @property \common\models\User $parent
 * @property ActiveQuery children
 * @property Tree tree
 */
class User extends ActiveRecord implements IdentityInterface
{
    private $_qualification;

    use Entity;

    const STATUS_DELETED = 0;
    const STATUS_ACTIVE = 10;
    const STATUS_VERIFIED = 20;
    const STATUS_TRUSTED = 30;
    const STATUS_FAKE = 1; // for testing

    public static $statuses = [
        User::STATUS_FAKE => 'Тестовый',
        User::STATUS_ACTIVE => 'Зарегистрирован',
        User::STATUS_DELETED => 'Заблокирован',
        User::STATUS_VERIFIED => 'Проверенный',
        User::STATUS_TRUSTED => 'Доверенный',
    ];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%user}}';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return ArrayHelper::merge(static::rulesPassword('password'), [
            [['username', 'email', 'perfect'], 'required'],
            ['username', 'string', 'min' => 2, 'max' => 32],
            ['email', 'email'],
            [['username', 'email'], 'filter', 'filter' => 'trim'],
            [['first', 'last'], 'string', 'min' => 3, 'max' => 16],
            [['first', 'last'], 'match', 'pattern'
            => "/^[\\wАБВГДЕЖЗИЙКЛМНОПРСТУФХЦЧШЩЬЭЮЯІЇҐабвгдежзийклмнопрстуфхцчшщьюя ёъэыіїґ']+$/"],
            ['skype', 'match', 'pattern' => '/^[a-z][a-z0-9\.,\-_]{5,31}$/i'],
            ['phone', 'number'],
            ['perfect', 'match', 'pattern' => '/^U\d{7}$/', 'message' => 'Формат должен быть U1234567'],
            ['status', 'default', 'value' => self::STATUS_ACTIVE],
            ['status', 'in', 'range' => array_keys(static::$statuses)],
            [['username', 'email', 'skype', 'phone', 'perfect'], 'unique',
                'targetClass' => User::className(),
                'message' => Yii::t('app', 'Пользовтель с данным значением уже существует')],
            [['status', 'account'], 'number', 'on' => 'manage']
        ]);
    }

    public static function rulesPassword($name) {
        return [
            [$name, 'required'],
            [$name, 'string', 'min' => 6, 'max' => 255],
        ];
    }

    public function scenarios() {
        return [
            'default' => ['email', 'first', 'last', 'skype', 'phone'],
            'manage' =>  ['email', 'first', 'last', 'skype', 'phone', 'perfect', 'username', 'status', 'account']
        ];
    }

    public function attributeLabels() {
        return [
            'id' => 'ID',
            'username' => Yii::t('app', 'Username'),
            'email' => Yii::t('app', 'Email'),
            'first' => Yii::t('app', 'Имя'),
            'last' => Yii::t('app', 'Фамилия'),
            'phone' => Yii::t('app', 'Телефон'),
            'skype' => Yii::t('app', 'Skype'),
            'perfect' => Yii::t('app', 'Perfect Money wallet'),
            'account' => Yii::t('app', 'Account'),
            'created_at' => Yii::t('app', 'Created'),
            'updated_at' => Yii::t('app', 'Updated')
        ];
    }

    public function __toString() {
        return $this->username;
    }

    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id, 'status' => self::STATUS_ACTIVE]);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        return static::findOne(['username' => $username]);
    }

    /**
     * Finds user by password reset token
     *
     * @param string $token password reset token
     * @return static|null
     */
    public static function findByPasswordResetToken($token)
    {
        if (!static::isPasswordResetTokenValid($token)) {
            return null;
        }

        return static::findOne([
            'password_reset_token' => $token,
            'status' => self::STATUS_ACTIVE,
        ]);
    }

    /**
     * Finds out if password reset token is valid
     *
     * @param string $token password reset token
     * @return boolean
     */
    public static function isPasswordResetTokenValid($token)
    {
        if (empty($token)) {
            return false;
        }
        $expire = Yii::$app->params['user.passwordResetTokenExpire'];
        $parts = explode('_', $token);
        $timestamp = (int) end($parts);
        return $timestamp + $expire >= time();
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return boolean if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password_hash);
    }

    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password_hash = Yii::$app->security->generatePasswordHash($password);
    }

    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }

    /**
     * Generates new password reset token
     */
    public function generatePasswordResetToken()
    {
        $this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
    }

    /**
     * Removes password reset token
     */
    public function removePasswordResetToken()
    {
        $this->password_reset_token = null;
    }

    /**
     * @param string $username
     */
    public function setParent($username) {
        if (is_string($username)) {
            $parent = static::findOne(['username' => $username]);
            if ($parent) {
                $this->parent_id = $parent->id;
            }
        }
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParent() {
        return $this->hasOne(User::className(), ['id' => 'parent_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getChildren() {
        return $this->hasMany(User::className(), ['parent_id' => 'id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getTree() {
        return $this->hasOne(Tree::className(), ['user_id' => 'id']);
    }

    /**
     * @return User
     */
    public static function getAdmin() {
        return static::findOne(1);
    }

    public function transfer($amount) {
        $this->account += $amount;
        $this->saveEntity(null, 'Невозможно осуществить перевод денег пользователя ' . $this->username);
    }

    public function getQualification() {
        if (is_null($this->_qualification)) {
            $command = Yii::$app->db->createCommand('SELECT amount FROM qualification WHERE id = :id', [
                ':id' => $this->id
            ]);
            $command->execute();
            $value = -30;
            if ($command->pdoStatement->rowCount() > 0) {
                $value += (float)$command->pdoStatement->fetchColumn();
            }
            $this->_qualification = floor($value);
        }
        return $this->_qualification;
    }
}
