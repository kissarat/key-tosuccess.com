<?php
/**
 * Created by PhpStorm.
 * User: taras
 * Date: 6/11/15
 * Time: 8:56 PM
 */

namespace common\models;


use frontend\behaviors\IpBehavior;
use Yii;
use yii\db\ActiveRecord;

class Log extends ActiveRecord {
    const LOGIN = 1;
    const LOGOUT = 2;
    const LOGIN_INVALID = 3;

    public static $types = [
        Log::LOGIN => 'Вход',
        Log::LOGOUT => 'Выход',
        Log::LOGIN_INVALID => 'Неудачная попытка входа',
    ];

    public static function tableName() {
        return 'log';
    }

    public function behaviors() {
        return [
            IpBehavior::className()
        ];
    }

    public static function log($type, $user_id = null) {
        if (!$user_id) {
            $user_id = Yii::$app->user->id;
        }
        $log = new Log();
        $log->type = $type;
        $log->user_id = $user_id;
        $log->save();
    }

    public static function canLogin($user_id) {
        return static::find()->andWhere([
            'user_id' => $user_id,
            'type' => static::LOGIN_INVALID
        ])
            ->andWhere('time > DATE_SUB(NOW(), INTERVAL 5 MINUTE)')
            ->count() < 3;
    }
    
    public function attributeLabels() {
        return [
            'ip' => Yii::t('app', 'IP'),
            'type' => Yii::t('app', 'Type'),
            'time' => Yii::t('app', 'Time'),
        ];
    }
}
