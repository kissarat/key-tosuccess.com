<?php
return [
    'adminEmail' => 'lab_tas@ukr.net',
    'supportEmail' => 'support@example.com',
    'user.passwordResetTokenExpire' => 3600,
];
