<?php
/**
 * Created by PhpStorm.
 * User: taras
 * Date: 6/9/15
 * Time: 9:05 PM
 */

namespace frontend\behaviors;


use Yii;
use yii\base\ActionFilter;

class NoTokenValidation extends ActionFilter {
    public function beforeAction($action) {
//        file_put_contents('/tmp/history', json_encode(Yii::$app->controller->action->id));
        if (in_array(Yii::$app->controller->action->id, $this->only)) {
            Yii::$app->controller->enableCsrfValidation = false;
        }
        return parent::beforeAction($action);
    }
}