<?php
/**
 * Created by PhpStorm.
 * User: taras
 * Date: 6/7/15
 * Time: 11:11 PM
 */

namespace frontend\behaviors;


use Yii;
use yii\base\Behavior;
use yii\db\ActiveRecord;

class IpBehavior extends Behavior {
    public function events() {
        return [
            ActiveRecord::EVENT_BEFORE_INSERT => 'beforeSave',
            ActiveRecord::EVENT_BEFORE_UPDATE => 'beforeSave'
        ];
    }

    public function beforeSave($event) {
        $this->owner->ip = ip2long(Yii::$app->request->userIP);
    }
}