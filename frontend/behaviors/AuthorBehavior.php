<?php
/**
 * Created by PhpStorm.
 * User: taras
 * Date: 6/7/15
 * Time: 11:11 PM
 */

namespace frontend\behaviors;


use Yii;
use yii\base\Behavior;
use yii\db\ActiveRecord;

class AuthorBehavior extends Behavior {
    public function events() {
        return [
            ActiveRecord::EVENT_BEFORE_INSERT => 'beforeCreate'
        ];
    }

    public function beforeCreate() {
        $this->owner->author = Yii::$app->user->getId();
    }
}
