<?php
/**
 * Created by PhpStorm.
 * User: taras
 * Date: 6/8/15
 * Time: 6:00 AM
 */

namespace frontend\models;


use yii\db\ActiveQuery;

class Post extends Article {
    /*
    public static function find() {
        return new PostQuery(get_called_class());
    }
    */
}

class PostQuery extends ActiveQuery {
    public function init() {
        $this->andWhere(['name' => null]);
        parent::init();
    }
}
