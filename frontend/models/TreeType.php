<?php

namespace frontend\models;

use Yii;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "tree_type".
 *
 * @property integer $id
 * @property string $name
 * @property integer $degree
 * @property integer $priority
 * @property number $stake
 * @property number $fee
 *
 * @property Tree[] $trees
 * @property ActiveQuery levels
 */
class TreeType extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tree_type';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['name', 'string', 'min' => 3],
            [['degree', 'priority', 'stake', 'fee'], 'required'],
            [['degree', 'priority'], 'integer'],
            [['stake', 'fee'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'degree' => Yii::t('app', 'Degree'),
            'priority' => Yii::t('app', 'Priority'),
            'stake' => Yii::t('app', 'Stake'),
            'fee' => Yii::t('app', 'Fee'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTrees()
    {
        return $this->hasMany(Tree::className(), ['type_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLevels()
    {
        return $this->hasMany(TreeLevel::className(), ['type_id' => 'id']);
    }

    /**
     * @param $i
     * @return TreeLevel
     */
    public function level($i) {
        $levels = $this->levels;
        $n = count($levels);
        for($j = 0; $j < $n; $j++) {
            if ($i == $levels[$j]->level)
                return $levels[$j];
        }
        return null;
    }

    /**
     * @param integer $i
     * @return float
     */
    public function interest($i) {
       $level = $this->level($i);
       return $level ? $level->interest : null;
    }

    public function __toString() {
        return $this->name;
    }
}
