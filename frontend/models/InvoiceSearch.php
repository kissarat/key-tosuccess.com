<?php

namespace frontend\models;

use Yii;
use yii\data\ActiveDataProvider;

/**
 * InvoiceSearch represents the model behind the search form about `frontend\models\Invoice`.
 */
class InvoiceSearch extends InvoiceUser
{
    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        //@todo Hard query
        $query = InvoiceUser::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => ['created_at' => SORT_DESC]
            ],
            'pagination' => [
                'pageSize' => 15,
            ],
        ]);

        foreach(['ip', 'status'] as $key) {
            if (isset($params[$key])) {
                $query->andWhere([$key => $params[$key]]);
            }
        }

        if (isset($params['month'])) {
            $query->andWhere('from_unixtime(updated_at) > (DATE_SUB(CURDATE(), INTERVAL 1 MONTH))');
        }

        if (isset($params['show'])) {
            switch($params['show']) {
                case 'payment':
                    $query->andWhere('amount > 0');
                    break;
                case 'withdraw':
                    $query->andWhere('amount < 0');
                    break;
                case 'waiting':
                    $query->orWhere(['<>', 'status', 'SUCCESS']);
                    $query->orWhere('status is null');
                    break;
            }
        }

        if (!Yii::$app->user->can('manage')) {
            $query->andWhere(['user_id' => Yii::$app->user->id]);
        }

        return $dataProvider;
    }
}
