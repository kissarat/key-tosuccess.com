<?php

namespace frontend\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "tree_level".
 *
 * @property integer $id
 * @property integer $type_id
 * @property integer $level
 * @property float interest
 *
 * @property TreeType $type
 */
class TreeLevel extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tree_level';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type_id', 'level', 'fee'], 'required'],
            [['type_id', 'level'], 'integer'],
            [['interest'], 'float']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'type_id' => Yii::t('app', 'Type ID'),
            'level' => Yii::t('app', 'Level'),
            'interest' => Yii::t('app', 'Interest'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getType()
    {
        return $this->hasOne(TreeType::className(), ['id' => 'type_id']);
    }
}
