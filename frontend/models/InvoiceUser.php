<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "invoice_user".
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $username
 * @property string $amount
 * @property string $status
 * @property string $wallet
 * @property integer $updated_at
 * @property integer $ip
 */
class InvoiceUser extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'invoice_user';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'user_id', 'username', 'amount', 'status', 'wallet', 'updated_at'], 'required'],
            [['id', 'user_id', 'updated_at', 'ip'], 'integer'],
            [['username', 'status'], 'string'],
            [['amount'], 'number'],
            [['wallet'], 'string', 'max' => 8]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'username' => 'Username',
            'amount' => 'Amount',
            'status' => 'Status',
            'wallet' => 'Wallet',
            'updated_at' => 'Updated At',
            'ip' => 'Ip',
        ];
    }
}
