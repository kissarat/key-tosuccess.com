<?php
namespace frontend\models;

use common\models\User;
use yii\base\Model;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * Signup form
 */
class SignupForm extends User
{
    private $_password;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return ArrayHelper::merge(parent::rules(), [
            ['parent_id', 'integer']
        ]);
    }

    public function scenarios() {
        return [
            'default' => ['username', 'password', 'parent_id', 'email', 'first', 'last', 'skype', 'phone', 'perfect']
        ];
    }

    /**
     * Signs user up.
     *
     * @return User|null the saved model or null if saving fails
     */
    public function signup()
    {
        $this->generateAuthKey();
        if (is_null($this->parent_id)) {
            $this->parent_id = 1;
        }
        if ($this->save()) {
            return $this;
        }
        return null;
    }

    public function attributeLabels() {
        return ArrayHelper::merge(parent::attributeLabels(), [
            'password' => Yii::t('app', 'Password'),
        ]);
    }

    public function setPassword($value) {
        parent::setPassword($value);
        $this->_password = $value;
    }

    public function getPassword() {
        return $this->_password;
    }
}
