<?php

namespace frontend\models;

use common\models\User;
use frontend\behaviors\IpBehavior;
use frontend\Entity;
use Yii;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "tree".
 *
 * @property integer $id
 * @property integer $type_id
 * @property integer $user_id
 * @property integer $status
 * @property number $deposit
 *
 * @property TreeType $type
 * @property User $user
 * @property ActiveQuery $members
 */
class Tree extends ActiveRecord
{
    use Entity;

    const OPEN = 51;
    const CLOSE = 52;
    const INSUFFICIENT_FUNDS = 61;
    const ADD_TO_CLOSED = 61;

    public static $statuses = [
        Tree::OPEN => 'Матрица открыта',
        Tree::CLOSE => 'Матрица закрыта',
        Tree::INSUFFICIENT_FUNDS => 'Недостаточно средств на счету',
        Tree::ADD_TO_CLOSED => 'Попытка добавиться в закрытую матрицу',
    ];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tree';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type_id', 'user_id', 'status'], 'required'],
            [['type_id', 'user_id', 'status'], 'integer'],
//            ['user_id', 'exists',
//                'targetClass' => User::className(),
//                'targetAttribute' => 'id'
//            ],
            ['deposit', 'number']
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            IpBehavior::className()
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'type_id' => Yii::t('app', 'Type ID'),
            'type' => Yii::t('app', 'Matrix'),
            'status' => Yii::t('app', 'Status'),
        ];
    }

    /**
     * @return \frontend\models\TreeType
     */
    public function getType()
    {
        return $this->hasOne(TreeType::className(), ['id' => 'type_id']);
    }

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return Tree
     */
    public function getParent()
    {
        if ($this->user->parent_id) {
            return Tree::findOne([
                'user_id' => $this->user->parent_id,
                'type_id' => $this->type_id
            ]);
        }
        return null;
    }

    /**
     * @return ActiveQuery
     */
    public function getMembers() {
        $command = Yii::$app->db->createCommand('SELECT t.* FROM user u
          JOIN tree t ON u.id = t.user_id WHERE u.parent_id = :parent_id', [
            ':parent_id' => $this->user_id
        ]);
        $command->execute();
        while($tree = $command->pdoStatement->fetchObject(static::className())) {
            yield $tree;
        }
    }

    public function countMembers() {
        $command = Yii::$app->db->createCommand('SELECT count(*) FROM user u
          JOIN tree t ON u.id = t.user_id WHERE u.parent_id = :parent_id', [
            ':parent_id' => $this->user_id
        ]);
        $command->execute();
        return $command->pdoStatement->fetchColumn();
    }

    public function fee() {
        for($current = $this->user, $i = 1; $current->parent_id; $current = $current->parent, $i++);
        return $this->type->fee($i);
    }

    public function __toString() {
        return $this->user . ' ' . $this->type->name;
    }
}
