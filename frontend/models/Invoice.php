<?php

namespace frontend\models;

use common\models\User;
use frontend\behaviors\AuthorBehavior;
use frontend\behaviors\IpBehavior;
use frontend\Entity;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "invoice".
 *
 * @property integer $id
 * @property string $status
 * @property number $amount
 * @property integer $batch
 * @property integer $sender_id
 * @property integer $receiver_id
 * @property \common\models\User $sender
 * @property \common\models\User $receiver
 * @property string $payer
 * @property string $payee
 * @property string $memo
 * @property integer $author
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $ip
 * @property bool isWithdraw
 */
class Invoice extends ActiveRecord
{
    use Entity;

    public static $statuses = [
        '' => 'Создано',
        'INVALID_AMOUNT' => 'Оплачена несоотвествующая сумма',
        'INVALID_RECEIVER' => 'Неправильний получатель',
        'INVALID_BATCH' => 'Несоотвествие номеров транзакций',
        'INVALID_RESPONSE' => 'Неизвестний ответ сервера',
        'INSUFFICIENT_FUNDS' => 'Недостаточно средств на счету пользователя',
        'NO_QUALIFICATION' => 'Пользователь не прошел квалификацию',
        'CANCEL' => 'Отмена',
        'FAIL' => 'Ошибка',
        'WITHDRAW_DENY' => 'Вывод денег не подтвержден администратором',
        'SUCCESS' => 'Осуществлено'
    ];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'invoice';
    }

    public function scenarios() {
        return [
            'payment' => ['amount', 'memo'],
            'withdraw' => ['amount', 'memo']
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['status', 'memo'], 'string'],
            [['amount', 'payer', 'payee'], 'required'],
            ['amount', 'number', 'min' => '0.01'],
            [['created_at', 'updated_at', 'ip'], 'integer'],
            [['payer', 'payee'], 'match', 'pattern' => '/^U\d{7}$/', 'message' => 'Формат должен быть U1234567']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'status' => Yii::t('app', 'Status'),
            'amount' => Yii::t('app', 'Amount'),
            'sender_id' => Yii::t('app', 'Sender'),
            'receiver_id' => Yii::t('app', 'Receiver'),
            'payer' => Yii::t('app', 'Sender'),
            'payee' => Yii::t('app', 'Receiver'),
            'memo' => Yii::t('app', 'Memo'),
            'created_at' => Yii::t('app', 'Created'),
            'updated_at' => Yii::t('app', 'Updated'),
            'ip' => Yii::t('app', 'IP'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            TimestampBehavior::className(),
            IpBehavior::className(),
        ];
    }

    public function getSender() {
        return $this->hasOne(User::className(), ['id' => 'sender_id']);
    }

    public function getReceiver() {
        return $this->hasOne(User::className(), ['id' => 'receiver_id']);
    }

    public static function updateStatus($id, $status) {
        Yii::$app->db->createCommand('UPDATE invoice SET status = :status WHERE id = :id', [
            ':id' => $id,
            ':status' => $status
        ])
            ->execute();
    }

    public function __toString() {
        $amount = $this->amount;
        $user = null;
        if ($this->isWithdraw) {
            $amount = -$amount;
            $user = $this->receiver;
        }
        else {
            $user = $this->sender;
        }
        return "$amount $user";
    }

    public function getIsWithdraw() {
        return 1 == $this->sender_id;
    }
}
