<?php
/**
 * Created by PhpStorm.
 * User: taras
 * Date: 6/10/15
 * Time: 11:41 PM
 */

namespace frontend\models;


use common\models\User;
use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;

class ChangePassword extends Model {
    public $old_password;
    public $new_password;

    public function rules() {
        return ArrayHelper::merge(
            User::rulesPassword("old_password"),
            User::rulesPassword("new_password")
        );
    }

    public function attributeLabels() {
        return [
            'old_password' => Yii::t('app', 'Старый пароль'),
            'new_password' => Yii::t('app', 'Новый пароль'),
        ];
    }
}
