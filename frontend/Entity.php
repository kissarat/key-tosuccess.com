<?php
/**
 * Created by PhpStorm.
 * User: taras
 * Date: 6/14/15
 * Time: 5:27 PM
 */

namespace frontend;


trait Entity {
    public function saveEntity($code = null, $message = null) {
        if (!$this->save()) {
            throw new LogEvent($this, $code, $message);
        }
    }
}
