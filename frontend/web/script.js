"use strict";

function each(array, call) {
    return Array.prototype.forEach.call(array, call);
}

function inherit(child, parent, proto, descriptor) {
    if (!child)
        child = function() {
            parent.apply(this, arguments);
        };
    if (!descriptor)
        descriptor = {};
    descriptor.base = {
        value: parent,
        enumerable: false,
        writable: false
    };
    child.prototype = Object.create(parent.prototype);
    child.prototype.constructor = child;
    var names = proto ? Object.getOwnPropertyNames(proto) : [];
    for (var i in names) {
        var name = names[i];
        descriptor[name] = Object.getOwnPropertyDescriptor(proto, name);
    }
    Object.defineProperties(child.prototype, descriptor);
    child.descriptor = descriptor;
    return child;
}

function login(username) {
    if (/\([^\)]+\)/.test($('[data-method]').html())) {
        $.get('/site/logout');
    }
    else {
        $.post('/site/login', {
            'LoginForm[username]': username,
            'LoginForm[password]': '1234567kipr',
            'LoginForm[rememberMe]': 1
        },
        function() {
            location.href = '/tree/matrix';
        });
    }
    //$('#loginform-username').val(username);
    //$('#loginform-password').val('1234567kipr');
    //$('form').submit();
}

var descendant_number = 0;

function Member(row) {
    this.id = row[1];
    this.parent_id = row[2];
    this.number = ++descendant_number;
    this.time = new Date(row[3] * 1000).toLocaleDateString();
    this.username = row[4];
    this.email = row[5];
    this.skype = row[6];
}

function Level(number, interest) {
    Array.call(this);
    this.number = number;
    this.interest = interest;
}

inherit(Level, Array, {
    get income() {
        return '$' + Math.floor(this.length * this.interest * 30);
    },

    get max() {
        return Math.pow(3, this.number);
    },

    get members() {
        return this.length + ' / ' + this.max;
    },

    get percent() {
        return (Math.round(this.interest * 1000)/10) + '%';
    }
});

var $matrix = $('#matrix');
if ($matrix.length > 0) {
    var levels = [
        new Level(1, 0.5),
        new Level(2, 0.015),
        new Level(3, 0.015),
        new Level(4, 0.015),
        new Level(5, 0.25)
    ];
    for (var i = 1; i < tree.length; i++) {
        var row = tree[i];
        levels[row[0] - 1].push(new Member(row));
    }

    $matrix.DataTable({
        data: levels,
        paging: false,
        searching: false,
        ordering:  false,
        info:  false,
        columns: [
            {title: 'Уровень', data: 'number'},
            {title: 'Участников', data: 'length'},
            {title: 'Прибыль', data: 'income'},
            {title: 'с пользователя', data: 'percent'}
        ]
    });

    $matrix.find('td').click(function () {
        var tr = $(this.parentNode);
        var old = $matrix.find('.selected');
        if (tr == old[0]) {
            return;
        }
        old.removeClass('selected');
        tr.toggleClass('selected');

        var $level = $('#level');
        if (!$level.html()) {
            $level.DataTable({
                paging: false,
                searching: false,
                info:  false,
                language: {
                    emptyTable: 'Нет учасников'
                },
                columns: [
                    {title: 'Номер', data: 'number'},
                    {title: 'Регистрация', data: 'time'},
                    {title: 'Логин', data: 'username'},
                    {title: 'Email', data: 'email'},
                    {title: 'Skype', data: 'skype'}
                ]
            }).draw();
        }

        var data = $level.DataTable();
        each($level.find('tr'), function(tr) {
            data.row(tr).remove();
        });
        data.draw();
        data.rows.add(levels[tr.find('td:first-child').html() - 1]).draw();
    });
}

if (window.matchMedia && matchMedia('(min-width: 768px)').matches) {
    var $cabinet = $('#nav-cabinet');
    $cabinet.remove();
    $cabinet.empty();
    $cabinet.addClass('nav-cabinet');
    $('<img/>')
        .attr('src', '/image/login.png')
        .appendTo($cabinet);
    $('ul.navbar-nav').after($cabinet);
}

if ($('.site-index').length > 0) {
    var $background = $('#background');
    if ($background.length > 0) {
        $background
            .addClass('main')
            .append($('#top'));
        $('.container').removeClass('container');
    }
}
