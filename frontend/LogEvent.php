<?php
/**
 * Created by PhpStorm.
 * User: taras
 * Date: 6/14/15
 * Time: 5:22 PM
 */

namespace frontend;

use common\models\User;
use Exception;
use frontend\models\Invoice;
use frontend\models\Tree;
use Yii;
use yii\base\Model;

class LogEvent extends Exception {
    const MODEL_LOG = 1;
    const MODEL_USER = 2;
    const MODEL_INVOICE = 3;
    const MODEL_TREE = 4;


    public static function models() {
        return [
            LogEvent::MODEL_LOG => get_called_class(),
            LogEvent::MODEL_USER => User::className(),
            LogEvent::MODEL_INVOICE => Invoice::className(),
            LogEvent::MODEL_TREE => Tree::className(),
        ];
    }

    private $_entity;
    public $model_type;
    public $code;

    public function __construct(Model $entity, $code = null, $message = null) {
        parent::__construct($message);
        $this->entity = $entity;
        $this->code = $code;
        Yii::error($message);
    }

    /**
     * @return Model
     */
    public function getEntity() {
        if ($this->_entity) {
            return $this->_entity;
        }
        return null;
    }

    public function setEntity(Model $value) {
        $this->_entity = $value;
    }

    public function log() {
//        $message = [$this->entity->className(), $this->entity->__toString()];
//        if ($this->message) {
//            $message[] = $this->message;
//        }
//        if (!empty($this->entity->errors)) {
//            $message[] = json_encode($this->entity->errors, JSON_UNESCAPED_UNICODE);
//        }
//        $message = implode(' ', $message);
//        file_put_contents('/tmp/log', $message . "\n", FILE_APPEND);
//        return parent::save($runValidation, $attributeNames);
    }
}
