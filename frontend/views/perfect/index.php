<?php

use frontend\models\Invoice;
use yii\grid\ActionColumn;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\InvoiceSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Invoices');
$this->params['breadcrumbs'][] = $this->title;
$show = Yii::$app->request->getQueryParam('show', '');
$show_time = Yii::$app->request->getQueryParam('month', '') <= 0;
if ($show_time) {
    $show_time = Html::a('последний месяц', ArrayHelper::merge(['index'], $_GET, ['month' => 1]));
}
?>
<div class="invoice-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php //echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?php if(!Yii::$app->user->can('manage')): ?>
            <?= Html::a(Yii::t('app', 'Пополнить'), ['create'], ['class' => 'btn btn-success']) ?>
            <?= Html::a(Yii::t('app', 'Вывести'), ['create', 'mode' => 'withdraw'], ['class' => 'btn btn-error']) ?>
        <?php endif ?>
    </p>
    <p>
        Показать:
        <?= Html::a(Yii::t('app', 'все'), ['perfect/index'],
            ['class' => 'btn']) ?>
        <?= Html::a(Yii::t('app', 'пополнение'),
            'payment' != $show ? ['perfect/index', 'show' => 'payment'] : '',
            ['class' => 'btn']) ?>
        <?= Html::a(Yii::t('app', 'выводы'),
            'withdraw' != $show ? ['perfect/index', 'show' => 'withdraw'] : '',
            ['class' => 'btn']) ?>
        <?= $show_time ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
//        'filterModel' => $searchModel,
        'columns' => [
            'id',
            'amount',
            [
                'attribute' => 'status',
                'format' => 'html',
                'value' => function($model) {
                    $out = Html::a(Invoice::$statuses[$model->status],
                        Url::to(['perfect/index', 'status' => $model->status]));
                    if ('SUCCESS' != $model->status && Yii::$app->user->can('manage')) {
                        if ($model < 0) {
                            $out .= ' ' . Html::a('вывести',
                                    ['withdraw', 'id' => $model->id],
                                    ['class' => 'btn btn-warning btn-xs']
                                );
                        }
                        else {
                            $out .= ' ' . Html::a('начислить',
                                    ['success', 'id' => $model->id],
                                    ['class' => 'btn btn-success btn-xs']
                                );
                        }
                    }
                    return $out;
                }
            ],
            [
                'label' => Yii::t('app', 'Payment'),
                'format' => 'html',
                'value' => function($model) {
                    return Html::a($model->username,
                        Url::to(['user/view', 'id' => $model->user_id]))
                    . " ($model->wallet)";
                }
            ],
            'updated_at:datetime',
            [
                'attribute' => 'ip',
                'label' => 'IP',
                'format'=> 'html',
                'value' => function($model) {
                    return Html::a(long2ip($model->ip),
                    Url::to(['perfect/index', 'ip' => $model->ip]));
                }
            ],

            [
                'class' => ActionColumn::className(),
                'template' => Yii::$app->user->can('manage') ? '{view} {update} {delete}' : '{view}'
            ],
        ],
    ]); ?>

</div>
<script>
    [].forEach.call(document.querySelectorAll('a[data-page]'), function(a) {
        a.href += '#w0';
    });
</script>
