<?php

use frontend\models\Invoice;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model frontend\models\Invoice */

$this->title = $model->__toString();
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Invoices'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="invoice-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?php
        if (Yii::$app->user->can('manage')) {
            echo Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']);
            echo ' ';
            echo Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                    'method' => 'post',
                ],
            ]);
        }
        if ('SUCCESS' != $model->status && Yii::$app->user->can('manage')) {
            echo ' ';
            if ($model->isWithdraw) {
                echo Html::a(Yii::t('app', 'Вывести'), ['withdraw', 'id' => $model->id], ['class' => 'btn btn-primary']);
            } elseif (Yii::$app->user->can('manage')) {
                echo Html::a(Yii::t('app', 'Начислить'), ['success', 'id' => $model->id], ['class' => 'btn btn-primary']);
            }
            echo ' ';
        }

        $status = Invoice::$statuses[$model->status] . ' ';
        if ('SUCCESS' != $model->status && '' != $model->status) {
            $status .= Html::a(Yii::t('app', 'Retry'),
                ['success', 'id' => $model->id], ['class' => 'btn btn-success btn-xs']);
        }
        ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            [
                'attribute' => 'status',
                'format' => 'html',
                'value' => $status
            ],
            'amount',
            [
                'label' => Yii::t('app', 'Sender'),
                'format' => 'raw',
                'value' => Html::a($model->sender->username, Url::to(['user/view', 'id' => $model->sender_id]))
                    . " ($model->payer)"
            ],
            [
                'label' => Yii::t('app', 'Receiver'),
                'format' => 'raw',
                'value' => Html::a($model->receiver->username, Url::to(['user/view', 'id' => $model->receiver_id]))
                    . " ($model->payee)"
            ],
            'memo:ntext',
            'created_at:datetime',
            'updated_at:datetime',
            [
                'label' => 'IP',
                'value' => long2ip($model->ip)
            ]
        ],
    ]) ?>

</div>
