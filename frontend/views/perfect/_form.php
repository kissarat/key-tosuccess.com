<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\models\Invoice */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="invoice-form">

    <?php $form = ActiveForm::begin();
    if (!$model->isNewRecord) {
        echo $form->field($model, 'status')->textInput();
    }
    ?>

    <input type="hidden" name="mode" value="<?=$model->scenario ?>" />
    <?= $form->field($model, 'amount')->textInput(['maxlength' => true]) ?>

    <?php
    if (Yii::$app->user->can('manage')) {
        $form->field($model, 'payer')->textInput(['maxlength' => true]);
        $form->field($model, 'payee')->textInput(['maxlength' => true]);
    }

    ?>

    <?= $form->field($model, 'memo')->textarea(['rows' => 6]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord
            ? Yii::t('app', 'payment' == $model->scenario ? 'Пополнить' : 'Вывести')
            : Yii::t('app', 'Обновить'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
