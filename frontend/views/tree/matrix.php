<?php
use yii\grid\GridView;
use yii\helpers\Html;

$this->title = Yii::t('app', 'Матрица');
$this->params['breadcrumbs'][] = $this->title;
echo Html::tag('script', 'var tree = ' . json_encode($models));
?>
<div class="tree-matrix">
    <h1><?= Html::encode($this->title) ?></h1>

    <?php if (count($models) == 0): ?>
        <?= Html::a(Yii::t('app', 'Open'), ['tree/open'], ['class' => 'btn btn-primary']); ?>
    <?php endif ?>
    <table id="matrix"></table>
    <table id="level"></table>
</div>
