<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = Yii::t('app', 'Открытие матрицы');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tree-open">
    <h1><?= Html::encode($this->title) ?></h1>

    <?php $form = ActiveForm::begin() ?>

    <?= Html::hiddenInput('agree', true) ?>

    <?= Html::submitButton(Yii::t('app', 'Open'), ['class' => 'btn btn-success']) ?>

    <?php ActiveForm::end() ?>
</div>
