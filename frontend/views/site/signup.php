<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\SignupForm */

$this->title = Yii::t('app', 'Signup');

$this->params['breadcrumbs'][] = $this->title;
if (Yii::$app->user->isGuest):
?>
<div class="site-signup">
    <h1><?= Html::encode($this->title) ?></h1>

    <p><?=Yii::t('app', 'Please fill out the following fields') ?>:</p>

    <div class="row">
        <div class="col-lg-5">
            <?php $form = ActiveForm::begin(['id' => 'form-signup']);
            if ($model->parent_id):
                echo '<div class="form-group">';
                echo Html::activeHiddenInput($model, 'parent_id');
                echo "<div>Реферал: " . Html::a($model->parent, ['user/view', 'id' => $model->parent_id]) . "</div>";
                echo '</div>';
            endif;
            foreach(['username', 'email', 'first', 'last', 'skype', 'phone', 'perfect'] as $name)
                echo $form->field($model, $name);
            echo $form->field($model, 'password')->passwordInput();
            ?>
                <div class="form-group">
                    <?= Html::submitButton($this->title, ['class' => 'btn btn-primary', 'name' => 'signup-button']) ?>
                </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
<?php else: ?>
    Это реферальная страница регистрации пользователя
    <?= Html::a(Yii::$app->user->identity, ['user/view', 'id' => Yii::$app->user->id]) ?>,
    для регистрации необходимо
    <?= Html::a('выйти', ['logout', 'go' => Yii::$app->user->identity->username]) ?>
<?php endif;
