<?php
/* @var $this yii\web\View */
use yii\helpers\Html;

$this->title = 'Key toSuccess';
?>
<div class="site-index">
    <?php if (Yii::$app->user->isGuest): ?>
        <!--top-->
        <div id="top">
            <div class="inner">
                <div class="region region-top">
                    <div id="block-block-1" class="block block-block block-odd first clearfix">

                    </div> <div id="block-block-3" class="block block-block block-even clearfix">

                        <div class="content" >
                            <h2 class="rtecenter">Лучше Быть Последним<br />
                                в списке МИЛЛИОНЕРОВ,</h2>
                            <h3 class="rtecenter">чем первым в списке<br />
                                лучших РАБотников месяца!</h3>
                        </div>


                    </div> <div id="block-block-14" class="block block-block block-odd last clearfix">




                        <div class="content" >
                            <p><a class="reg" href="/site/signup">Регистрация</a></p>
                        </div>


                    </div>   </div>

            </div>
        </div>
        <!--top end-->
    <?php endif ?>

    <!--mission-->
    <div id="mission">
        <div class="inner">
            <div class="region region-mission">
                <div id="block-block-4" class="block block-block block-odd first last clearfix">

                    <div class="content" >
                        <table border="0" cellpadding="0" cellspacing="0" class="mission" style="width: 100%;"><tbody><tr><th>
                                    <h2>КЛЮЧ К УСПЕХУ</h2>
                                    <h3>Миссия проекта</h3>
                                </th>
                                <td>
                                    <p>Мы гарантируем, что при Вашем активном участии Ваш стиль жизни кардинально изменится. Мы заинтересованы в Вашем успехе, поскольку мы понимаем, что, чем большему количеству людей мы поможем реализовать свои желания и цели, тем более успешней будет наш проект.<br />
                                        Мы дарим Вам нашу идею и верим, что она поможет Вам и Вашим близким решить свои финансовые проблемы и обрести уверенность в завтрашнем дне, ведь именно финансовое благополучие является одним из самых важных факторов успеха.</p>
                                    <p>С Уважением Команда Key to Success.</p>
                                </td>
                            </tr></tbody></table>    </div>


                </div>   </div>

        </div>
    </div>
    <!--mission end-->
    <!--events-->
    <div id="events">
        <div class="inner">
            <div class="region region-events">
                <div id="block-views-events-block" class="block block-views block-odd first last clearfix">
                    <h2 class="block-title">Новости</h2>
                    <div class="content">
                        <div class="view">
                            <?php foreach($news as $post): ?>
                                <div class="post">
                                    <span class="date"><?= date('d.m.Y', $post->created_at) ?></span>
                                    <div class="title">
                                        <?= Html::a($post->title, ['post/view', 'id' => $post->id]) ?>
                                    </div>
                                    <div class="body"><?= substr(strip_tags($post->content), 0, 400) ?></div>
                                    <div class="details">
                                        <?= Html::a('подробнее >>', ['post/view', 'id' => $post->id]) ?>
                                    </div>
                                </div>
                            <?php endforeach ?>
                        </div>
                        <div class="more-link">
                            <a href="/post/index">все новости</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--events end-->

    <!--priorities-->
    <div id="priorities">
        <div class="inner">
            <h2 class="block-title">Преимущества</h2>
            <div class="content">
                <div class="marketing">Доступный маркетинговый план</div>
                <div class="withdraw">Быстрый возврат первых инвестиций</div>
                <div class="stability">Стабильный доход в ближайшем будущем</div>
                <div class="support">Качественная техническая поддержка</div>
            </div>
        </div>
    </div>
    <!--priorities end-->
    <!--video-->
    <div id="video">
        <div class="inner">
            <iframe src="http://www.youtube.com/embed/vOrtSIsomqg?origin=http://elite-monitor.ru">
            </iframe>
            <div class="region region-video">
                <h2>Успех не придет к тебе...
                    Ты придешь к нему!</h2>

                <!--div class="content" >
                    <p>Proin vel nibh non sapien viverra gravida eu in velit. Aliquam et ipsum ac nisl tempus blandit eget et nulla. Aenean rhoncus congue gravida. Donec ac leo mauris, et mattis felis. Proin sed elementum ipsum. In hendrerit, tortor in fermentum volutpat, tortor dui faucibus sem, vel lobortis magna purus non dolor. Maecenas tortor lorem, pulvinar nec lobortis sed, auctor sit amet sapien. In semper molestie dui a consequat.</p>
                </div-->
            </div>
        </div>
    </div>
    <!--video end-->
    <!--quotes-->
    <div id="quotes">
        <div class="inner">
            <h3>"Если у тебя мало денег –<br />
                надо делать бизнес.<br />
                Если денег нет совсем – надо делать бизнес срочно, прямо сейчас."</h3>
            <h4>Джон Дэвисон Рокфеллер</h4>
        </div>
    </div>
    <!--quotes end-->

    <!--prices-->
    <div id="prices">
        <div class="inner">
            <div>
                <h2>Наши цены</h2>
                <div>
                    <p>
                        За 30$ у каждого человека есть гарантированная возможность стать богатым и
                        независимым человеком, достичь своих целей, воплотить свои мечты и прожить\
                        жизнь так, как он этого хочет.
                    </p>
                    <h3>Мы принимаем:</h3>
                    <p><img alt="" src="/image/perfectmoney.png" /></p>
                </div>
            </div>

            <div>
                <div class="price-box">
                    <div class="price-box_header">
                        <div class="price-box_header_cnt">
                            <h4>Персональный</h4>
                            <div class="price">
                                $30
                                <span class="cent">.00</span>
                                <span class="period">/мес.</span>
                            </div>
                        </div>
                    </div>
                    <div class="price-box_body">
                        <p class="button">
                            <a href="#">Оплатить</a>
                        </p>
                    </div>
                </div>
                <div class="price-box">
                    <div class="price-box_header">
                        <div class="price-box_header_cnt">
                            <h4>Бизнес</h4>
                            <div class="price">
                                $300
                                <span class="cent">.00</span>
                                <span class="period">/мес.</span>
                            </div>
                        </div>
                    </div>
                    <div class="price-box_body">
                        <p class="button inactive">
                            <a>Скоро!</a>
                        </p>
                    </div>
                </div>
                <div class="price-box">
                    <div class="price-box_header">
                        <div class="price-box_header_cnt">
                            <h4>VIP</h4>
                            <div class="price">
                                $1000
                                <span class="cent">.00</span>
                                <span class="period">/мес.</span>
                            </div>
                        </div>
                    </div>
                    <div class="price-box_body">
                        <p class="button inactive">
                            <a>Скоро!</a>
                        </p>
                    </div>
                </div>
            </div>

        </div>
    </div>
    <!--prices end-->
</div>
