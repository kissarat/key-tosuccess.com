<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Users');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-index">

    <?php if (isset($_GET['referer'])): ?>
        <h1>Комманда пользователя <?= $_GET['referer'] ?></h1>
    <?php else: ?>
        <h1><?= Html::encode($this->title) ?></h1>
    <?php endif;
    // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?php
        if (Yii::$app->user->can('manage')) {
            Html::a(Yii::t('app', 'Create'), ['create'], ['class' => 'btn btn-success']);
        }
        ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            'id',
            [
                'attribute' => 'username',
                'format' => 'html',
                'value' => function($model) {
                    return Html::a($model->username, ['view', 'id' => $model->id]);
                }
            ],
            'email:email',
            // 'status',
            // 'created_at',
            // 'updated_at',
            'perfect',
            'account',

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => Yii::$app->user->can('manager') ? '{view} {update} {delete}' : '{view}'
            ],
        ],
    ]); ?>

</div>
