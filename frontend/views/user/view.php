<?php

use common\models\User;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\User */

$this->title = $model->username;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Users'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$team = [];
foreach($model->children as $child) {
    $team[] = Html::a($child, ['view', 'id' => $child->id]);
}

$user = Yii::$app->user;
$me = $model->id == $user->id;

$account = $model->account . ' ';
$q = - $model->qualification;
$account .= $q > 0
    ? Html::a("внесите $$q для квалификации",
        ['perfect/create', 'amount' => $q], [
            'class' => 'btn btn-danger btn-xs',
            'title' => 'Выводить деньги можно только имея квалификацию'
        ])
    : Html::tag('span', 'получил квалификацию', ['class' => 'label label-success']);
?>
<div class="user-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?php
        if ($user->id == $model->id || $user->can('manage')): ?>
            <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']);
        endif;
        if ($user->can('manage')): ?>
            <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                    'method' => 'post',
                ],
            ]); ?>
            <?= Html::a(Yii::t('app', 'Изменить пароль'),
                ['site/reset-password', 'id' => $model->id], ['class' => 'btn btn-primary']);
        elseif ($me): ?>
            <?= Html::a(Yii::t('app', 'Пополнить счет'), ['perfect/create'], ['class' => 'btn btn-success']); ?>
            <?= Html::a(Yii::t('app', 'Изменить пароль'), ['change'], ['class' => 'btn btn-primary']);
        endif ?>
        <?= Html::a(Yii::t('app', 'Команда'),
            ['index', 'referer' => $model->username],
            ['class' => 'btn btn-primary']); ?>
        <?php
        if ($me ||  $user->can('manage')) {
            echo Html::a(Yii::t('app', 'Журнал'),
                ['history', 'id' => $model->id],
                ['class' => 'btn btn-primary']);
        }
        ?>
    <div class="form-group">
        <?php
            $referer = ['site/signup', 'referer' => $model->username];
            echo Html::a('Реферальная ссылка', $referer, ['class' => 'form-label']);?>

        <input class="form-control" value="<?= Url::to($referer, true); ?>">
    </div>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            [
                'attribute' => 'account',
                'format' => 'html',
                'value' => $account
            ],
            [
                'attribute' => 'status',
                'label' => Yii::t('app', 'Status'),
                'value' => User::$statuses[$model->status]
            ],
            [
                'label' => 'Команда',
                'format' => 'html',
                'value' => implode(', ', $team)
            ],
            'email:email',
            'first',
            'last',
            'phone',
            'skype',
            'perfect',
            'created_at:datetime',
            'updated_at:datetime',
            [
                'label' => 'Реферал',
                'format' => 'html',
                'value' => $model->parent_id ? Html::a($model->parent, ['user/view', 'id' => $model->parent_id]) : null
            ]
        ],
    ]);
    ?>
</div>
