<?php

use common\models\Log;
use frontend\models\Tree;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel common\models\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Journal');
$this->params['breadcrumbs'][] = ['label' => $user, 'url' => ['view', 'id' => $user->id]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-journal">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            'time',
            [
                'attribute' => 'type',
                'contentOptions' => function($model) {
                    if (Log::LOGIN_INVALID == $model->type) {
                        return [
                            'class' => 'danger'
                        ];
                    }
                    return [];
                },
                'value' => function($model) {
                    return isset(Log::$types[$model->type]) ? Log::$types[$model->type] : Tree::$statuses[$model->type];
                }
            ],
            [
                'attribute' => 'ip',
                'label' => 'IP',
                'format'=> 'html',
                'value' => function($model) {
                    return Html::a(long2ip($model->ip),
                        Url::to(['perfect/index', 'ip' => $model->ip]));
                }
            ],
        ],
    ]); ?>

</div>
