<?php

use common\models\User;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\User */
/* @var $form yii\widgets\ActiveForm */

?>

<div class="user-form">

    <?php

    $form = ActiveForm::begin();

    if ('manage' == $model->scenario) {
        echo $form->field($model, 'username');
        echo $form->field($model, 'account');
        echo $form->field($model, 'status')->dropDownList(User::$statuses);
        echo $form->field($model, 'perfect');
    }

    foreach(['email', 'first', 'last', 'skype', 'phone'] as $name)
        echo $form->field($model, $name);
    ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord
            ? Yii::t('app', 'Create')
            : Yii::t('app', 'Save'),
            ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
