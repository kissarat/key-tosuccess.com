<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\User */

$this->title = $model . ' :: '. Yii::t('app', 'Editing');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Users'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Editing');
?>
<div class="user-update">

    <h1><?= Html::encode(Yii::t('app', 'Editing')) ?></h1>
    <div class="form-group">
    <?php
    if ($model->id == Yii::$app->user->id) {
        echo Html::a(Yii::t('app', 'Пополнить счет'), ['perfect/create'], ['class' => 'btn btn-success']);
        echo ' ';
        echo Html::a(Yii::t('app', 'Изменить пароль'), ['user/change'], ['class' => 'btn btn-primary']);
    } ?>
    </div>

    <?= $this->render('_form', [
        'model' => $model,
    ])
    ?>
</div>
