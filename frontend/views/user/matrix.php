<?php
use common\models\User;
use yii\helpers\Html;

function node(User $user) {
    $children = [];
    foreach($user->children as $child)
        $children[] = node($child);
    $json = [
        'user_id' => $user->id,
        'name' => $user->__toString()
    ];
    if (!empty($children)) {
        $json['children'] = $children;
    }
    return $json;
}

$json = node($model);
$json = json_encode($json, JSON_PRETTY_PRINT);

echo '<script src="//cdnjs.cloudflare.com/ajax/libs/d3/3.5.5/d3.min.js" charset="utf-8"></script>';
echo "\n<script>\nvar json = $json;\n</script>\n";

echo Html::a(Yii::t('app', 'Open'), ['tree/open'], ['class' => 'btn btn-primary']);
?>
<div id="tree"></div>
<style type="text/css" id="tree-style">
    .node {
        cursor: pointer;
    }

    .node circle {
        fill: #fff;
        stroke: #999999;
        stroke-width: 2px;
    }

    .node text {
        font: 15px sans-serif;
    }

    .link {
        fill: none;
        stroke: #ccc;
        stroke-width: 2px;
    }
</style>
<script>
    var username = '<?= Yii::$app->user->identity->username ?>';

    function delay(node, i, timeout) {
        if (!timeout)
            timeout = 380;
        setTimeout(function() {
            if (!node.children) {
                return
            }
            var child = node.children[i];
            if (child) {
                click(child);
                delay(node, i + 1);
            }
            else {
                for(var j = 0; j < 3; j++) {
                    setTimeout((function() {
                        delay(this, 0, 0);
                    }).bind(node.children[j]), j * 380 * 3 + 20)
                }
            }
        }, timeout);
    }

    function populate(node, j) {
        if (node.children) {
            for (var i = 0; i < 3; i++) {
                var child = node.children[i];
                if (child) {
                    if (!child.children)
                        child.children = [];
                    populate(child, j + 1)
                } else {
                    child = node.children[i] = {name: '?'};
                    if (j < 3) {
                        child.children = [];
                        populate(child, j + 1);
                    }
                }
            }
        }
    }

//    populate(json, 1);
    delay(json, 0);

    var margin = {top: 0, right: 20, bottom: 20, left: 120},
        width = 1200 - margin.right - margin.left,
        height = 1200 - margin.top - margin.bottom;

    var i = 0,
        duration = 300,
        root;

    var tree = d3.layout.tree()
        .size([height, width]);

    var diagonal = d3.svg.diagonal()
        .projection(function(d) { return [d.y, d.x]; });

    var svg = d3.select("#tree").append("svg")
        .attr("width", width + margin.right + margin.left)
        .attr("height", height + margin.top + margin.bottom)
        .append("g")
        .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

    root = json;
    root.x0 = height / 2;
    root.y0 = 0;

    function collapse(d) {
        if (d.children) {
            d._children = d.children;
            d._children.forEach(collapse);
            d.children = null;
        }
    }

    root.children.forEach(collapse);
    update(root);

    d3.select(self.frameElement).style("height", "800px");

    function update(source) {

        // Compute the new tree layout.
        var nodes = tree.nodes(root).reverse(),
            links = tree.links(nodes);

        // Normalize for fixed-depth.
        nodes.forEach(function(d) { d.y = d.depth * 120; });

        // Update the nodes…
        var node = svg.selectAll("g.node")
            .data(nodes, function(d) { return d.id || (d.id = ++i); });

        // Enter any new nodes at the parent's previous position.
        var nodeEnter = node.enter().append("g")
            .attr("class", "node")
            .attr("transform", function(d) { return "translate(" + source.y0 + "," + source.x0 + ")"; })
            .on("click", click);

        nodeEnter.append("circle")
            .attr("r", 1e-6)
            .style("fill", function(d) { return d._children ? "lightsteelblue" : "#fff"; });

        nodeEnter.append('a')
            .attr('xlink:href', function(d) {
                if (d.user_id) {
                    return '/user/view?id=' + d.user_id;
                }
                return '/site/signup?referer=' + username;
            })
            .attr('target', '_blank')
            .append("text")
            .attr("x", function(d) { return d.children || d._children ? -20 : 20; })
            .attr("dy", ".35em")
            .attr("text-anchor", function(d) { return d.children || d._children ? "end" : "start"; })
            .text(function(d) { return d.name; })
            .style("fill-opacity", 1e-6);

        // Transition nodes to their new position.
        var nodeUpdate = node.transition()
            .duration(duration)
            .attr("transform", function(d) { return "translate(" + d.y + "," + d.x + ")"; });

        nodeUpdate.select("circle")
            .attr("r", 8)
            .style("fill", function(d) { return d._children ? "#bbb" : "#fff"; });

        nodeUpdate.select("text")
            .style("fill-opacity", 1);

        // Transition exiting nodes to the parent's new position.
        var nodeExit = node.exit().transition()
            .duration(duration)
            .attr("transform", function(d) { return "translate(" + source.y + "," + source.x + ")"; })
            .remove();

        nodeExit.select("circle")
            .attr("r", 1e-6);

        nodeExit.select("text")
            .style("fill-opacity", 1e-6);

        // Update the links…
        var link = svg.selectAll("path.link")
            .data(links, function(d) { return d.target.id; });

        // Enter any new links at the parent's previous position.
        link.enter().insert("path", "g")
            .attr("class", "link")
            .attr("d", function(d) {
                var o = {x: source.x0, y: source.y0};
                return diagonal({source: o, target: o});
            });

        // Transition links to their new position.
        link.transition()
            .duration(duration)
            .attr("d", diagonal);

        // Transition exiting nodes to the parent's new position.
        link.exit().transition()
            .duration(duration)
            .attr("d", function(d) {
                var o = {x: source.x, y: source.y};
                return diagonal({source: o, target: o});
            })
            .remove();

        // Stash the old positions for transition.
        nodes.forEach(function(d) {
            d.x0 = d.x;
            d.y0 = d.y;
        });
    }

    // Toggle children on click.
    function click(d) {
        if (d.children) {
            d._children = d.children;
            d.children = null;
        } else {
            d.children = d._children;
            d._children = null;
        }
        update(d);
    }

</script>
