<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\models\ChangePassword */
/* @var $form ActiveForm */

$this->title = 'Изменение пароля';
$this->params['breadcrumbs'][] = [
    'label' => Yii::t('app', 'Профиль'),
    'url' => ['view', 'id' => Yii::$app->user->id]
];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-change">

    <?php $form = ActiveForm::begin(); ?>

        <?= $form->field($model, 'old_password')->passwordInput() ?>
        <?= $form->field($model, 'new_password')->passwordInput() ?>
    
        <div class="form-group">
            <?= Html::submitButton(Yii::t('app', 'Submit'), ['class' => 'btn btn-primary']) ?>
        </div>
    <?php ActiveForm::end(); ?>

</div><!-- user-change -->
