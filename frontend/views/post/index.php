<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\LinkPager;
use yii\widgets\ListView;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\PostSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'News');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="post-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php /*echo $this->render('_search', ['model' => $searchModel]);*/ ?>

    <p>
        <?php if (Yii::$app->user->can('manage')) {
            echo Html::a(Yii::t('app', 'Create'), ['create'], ['class' => 'btn btn-success']);
        } ?>
    </p>

    <ul class="list-group">
    <?php
    echo ListView::widget([
        'dataProvider' => $dataProvider,
        'itemView' => function($model) {
            echo Html::tag('li', Html::a($model->title, ['view', 'id' => $model->id]),
                ['class' => 'list-group-item']);
        }
    ]);
    ?>
    </ul>

</div>
