<?php
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use frontend\assets\AppAsset;
use frontend\widgets\Alert;

/* @var $this \yii\web\View */
/* @var $content string */

AppAsset::register($this);
$guest = Yii::$app->user->isGuest ? 'guest' : '';
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>

    <meta name="generator" content="WordPress 3.6.1" />
    <?php $this->head() ?>
</head>
<body class="<?= $guest ?>">
<?php $this->beginBody() ?>
<div class="wrap">
    <?php
    NavBar::begin([
        'options' => [
            'class' => 'navbar-inverse navbar-fixed-top',
        ],
    ]);
    $menuItems = [
        ['label' => Yii::t('app', 'Главная'), 'url' => ['/']],
        ['label' => Yii::t('app', 'News'), 'url' => ['/post/index']],
        ['label' => Yii::t('app', 'Marketing'), 'url' => ['/site/marketing']],
        ['label' => Yii::t('app', 'Вопрос-Ответ'), 'url' => ['/faq/index']],
        ['label' => Yii::t('app', 'Feedback'), 'url' => ['/site/contact']],
    ];
    if (Yii::$app->user->isGuest) {
        $menuItems[] = ['label' => Yii::t('app', 'Регистрация'), 'url' => ['/site/signup']];
        $menuItems[] = ['label' => Yii::t('app', 'Вход'), 'url' => ['/site/login'],
            'linkOptions' => ['id' => 'nav-cabinet']];
    } else {
        if (Yii::$app->user->can('manage')) {
            $menuItems[] = ['label' => Yii::t('app', 'Пользователи'), 'url' => ['/user/index']];
        }
        $menuItems[] = ['label' => Yii::t('app', 'Профиль'), 'url' => ['/user/view']];
        $menuItems[] = ['label' => Yii::t('app', 'Матрица'), 'url' => ['/tree/matrix']];
        $menuItems[] = ['label' => Yii::t('app', 'Оплата'), 'url' => ['/perfect']];
        $menuItems[] = [
            'label' => Yii::t('app', 'Logout') . ' (' . Yii::$app->user->identity->username . ')',
            'url' => ['/site/logout'],
            'linkOptions' => [
                'data-method' => 'post',
                'id' => 'nav-cabinet'
            ]
        ];
    }


    echo Nav::widget([
        'options' => ['class' => 'navbar-nav'],
        'items' => $menuItems,
    ]);
    NavBar::end();
    ?>

    <?php if ($guest): ?>
    <div id="background">
        <img src="/image/logo.png" />
    </div>
    <?php endif ?>

    <div class="container">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= Alert::widget() ?>
        <?= $content ?>
    </div>
</div>

<!--footer class="footer">
    <div class="container">
        <p class="pull-left">Разработка <a href="http://zenothing.com">zenothing.com</a></p>
    </div>
</footer-->

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
