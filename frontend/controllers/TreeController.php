<?php

namespace frontend\controllers;

use common\models\Log;
use common\models\User;
use Exception;
use frontend\Entity;
use frontend\LogEvent;
use PDO;
use Yii;
use frontend\models\Tree;
use frontend\models\TreeSearch;
use yii\data\ArrayDataProvider;
use yii\web\Controller;
use yii\web\JqueryAsset;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * TreeController implements the CRUD actions for Tree model.
 */
class TreeController extends Controller
{
    use Entity;

    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Tree models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new TreeSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Tree model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Tree model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Tree();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Creates a new Tree model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     * @throws \yii\db\Exception
     */
    public function actionOpen()
    {
        $user = Yii::$app->user->identity;
        if (Tree::find()->where(['user_id' => $user->id])->count() > 0) {
            return $this->redirect('matrix');
        }
        if (isset($_POST['agree']) && 1 == $_POST['agree']) {
            $transaction = Yii::$app->db->beginTransaction();
            $model = new Tree();
            $model->user_id = $user->id;
            $model->type_id = 1;
            try {
                if ($user->account >= $model->type->stake) {
                    Yii::$app->db->createCommand('CALL enter(:user_id, 1)', [
                        ':user_id' => $user->id
                    ])->execute();
                    $transaction->commit();
                    Yii::$app->session->setFlash('success', 'Матрица открыта');
                    Log::log(Tree::CLOSE, $model->user_id);
                    return $this->redirect(['matrix', 'id' => $model->id]);
                }
                else {
                    throw new Exception('Недостаточно средств на счету');
                }

            }
            catch (Exception $ex) {
                $transaction->rollBack();
                if ($ex instanceof LogEvent) {
                    $ex->log();
                }
                Yii::$app->session->setFlash('error', $ex->getMessage());
            }
        }

        return $this->render('open');
    }

    /**
     * @return string
     * @throws \yii\db\Exception
     */
    public function actionMatrix()
    {
        $command = Yii::$app->db->createCommand('CALL descend(:user_id)', [
            ':user_id' => Yii::$app->user->id
        ]);
        $command->execute();
        $st = $command->pdoStatement;
        $models = $st->rowCount() > 0 ? $st->fetchAll(PDO::FETCH_NUM) : [];
        Yii::$app->view->registerJsFile('//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js', [
            'depends' => [JqueryAsset::className()]
        ]);
        Yii::$app->view->registerCssFile('//cdn.datatables.net/1.10.7/css/jquery.dataTables.min.css');
        return $this->render('matrix', [
            'models' => $models
        ]);
    }

    /**
     * Updates an existing Tree model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Tree model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Tree model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Tree the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Tree::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}

class TreeException extends Exception {
    public $id;
    public $code;

    public function __construct($id, $code, $message = null) {
        $this->id = $id;
        $this->code = $code;
        $this->message = Tree::$statuses[$code];
        if ($message) {
            $user_id = Yii::$app->user->id;
            Yii::error("$id\t$code\t$user_id\t$message", 'perfect');
        }
        Log::log($code);
    }
}
