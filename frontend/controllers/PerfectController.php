<?php

namespace frontend\controllers;

use common\models\User;
use frontend\behaviors\NoTokenValidation;
use frontend\models\InvoiceSearch;
use Yii;
use frontend\models\Invoice;
use yii\base\Exception;
use yii\filters\AccessControl;
use yii\helpers\Html;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * PerfectController implements the CRUD actions for Invoice model.
 */
class PerfectController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],

            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index', 'success', 'create', 'withdraw', 'payment', 'update', 'delete'],
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index', 'create'],
                        'roles' => ['@']
                    ],
                    [
                        'allow' => true,
                        'actions' => ['withdraw', 'success', 'update', 'delete'],
                        'roles' => ['admin']
                    ]
                ]
            ],

            'no_csrf' => [
                'class' => NoTokenValidation::className(),
                'only' => ['success', 'fail'],
            ]
        ];
    }

    /**
     * Lists all Invoice models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new InvoiceSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel
        ]);
    }

    /**
     * Displays a single Invoice model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);
        if (1 == $model->sender_id) {
            $model->scenario = 'withdraw';
        }
        if ('SUCCESS' != $model->status) {
            static::warnQualification($model);
        }
        return $this->render('view', [
            'model' => $model,
        ]);
    }

    public static function warnQualification($model) {
        if ('withdraw' == $model->scenario && !Yii::$app->user->can('manage')) {
            $q = Yii::$app->user->identity->qualification;
            if ($q < 0) {
                $q = - $q;
                Yii::$app->session->addFlash('warning', 'Для осуществления вывода вам необходимо подтвердить
                квалификацию - ' . Html::a('поплнить счет на $' . $q, ['create', 'amount' => $q]));
            }
        }
    }

    public static function warnManual() {
        if (!Yii::$app->user->can('manage')) {
            Yii::$app->session->addFlash('warning', 'Платежы осуществляються в ручном режыме');
        }
    }

    /**
     * Creates a new Invoice model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @param string $mode
     * @param null|number $amount
     * @return mixed
     */
    public function actionCreate($mode = 'payment', $amount = null)
    {
        $scenario = isset($post['mode']) ? $post['mode'] : $mode;
        if ('withdraw' != $scenario)
            $scenario = 'payment';
        $model = new Invoice(['scenario' => $scenario]);
        static::warnQualification($model);
        $post = Yii::$app->request->post();
        $admin = User::getAdmin();
        if ($model->load($post)) {
            if ('payment' == $model->scenario) {
                $model->sender_id = Yii::$app->user->identity->id;
                $model->payer = Yii::$app->user->identity->perfect;
                $model->receiver_id = $admin->id;
                $model->payee = $admin->perfect;
            }
            else {
                $model->sender_id = $admin->id;
                $model->payer = $admin->perfect;
                $model->receiver_id = Yii::$app->user->identity->id;
                $model->payee = Yii::$app->user->identity->perfect;
            }
            $account = Yii::$app->user->identity->account;
            if ('withdraw' == $model->scenario && $model->amount > $account) {
                $model->addError('amount', "Сумма превышает ваш баланс $account");
            }
            if ($model->save()) {
                $this->redirect(['view', 'id' => $model->id]);
            }
        } else {
            static::warnManual();
            if ($amount) {
                $model->amount = $amount;
            }
        }

        return $this->render('create', [
            'model' => $model
        ]);
    }

    /**
     * Updates an existing Invoice model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Invoice model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();
        return $this->redirect(['index']);
    }

    public function actionWithdraw($id) {
        $transaction = Yii::$app->db->beginTransaction();
        $invoice = $this->findModel($id);
        $invoice->scenario = 'withdraw';
        try {
            if ($invoice->amount > $invoice->receiver->account) {
                throw new PerfectException($id, 'INSUFFICIENT_FUNDS',
                    $invoice->amount . ' > ' . $invoice->receiver->account);
            }
            if ($invoice->receiver->qualification < 0) {
                throw new PerfectException($id, 'NO_QUALIFICATION', $invoice->receiver->qualification);
            }
            $invoice->status = 'SUCCESS';
            $invoice->receiver->account -= $invoice->amount;
            $invoice->receiver->saveEntity(null, "Невозможно изменить данные получателя "
                . $invoice->receiver->username . " при осуществлении перевода №" . $invoice->id);
            $invoice->saveEntity(null, 'Невозможно сохранить данные перевода №' . $invoice->id);
            Yii::$app->session->setFlash('success', "Оплата #$id осуществелна");
            $transaction->commit();
        }
        catch(Exception $ex) {
            $transaction->rollBack();
            Yii::$app->session->setFlash('error', $ex->getMessage());
        }
        return $this->render('view', [
            'model' => $invoice
        ]);
    }

    public function actionSuccess($id) {
        $transaction = Yii::$app->db->beginTransaction();
        $invoice = $this->findModel($id);
        $invoice->scenario = 'payment';
        if ('SUCCESS' == $invoice->status) {
            Yii::$app->session->setFlash('success', "Оплата #$id уже была осуществелна");
            return $this->render('view', [
                'model' => $invoice
            ]);
        }
        try {
            $invoice->status = 'SUCCESS';
            $invoice->sender->account += $invoice->amount;
            $invoice->save();
            $invoice->sender->save();
            Yii::$app->session->setFlash('success', "Оплата #$id осуществелна");
            $transaction->commit();
            return $this->render('view', [
                'model' => $invoice
            ]);
        }
        catch(Exception $ex) {
            $transaction->rollBack();
            Yii::$app->session->setFlash('error', $ex->getMessage());
        }
        return $this->render('result');
    }

    /**
     * Finds the Invoice model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Invoice the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Invoice::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}

class PerfectException extends Exception {
    public $id;
    public $code;

    public function __construct($id, $code, $message = null) {
        $this->id = $id;
        $this->code = $code;
        $this->message = Invoice::$statuses[$code] . " (перевод #$id)";
        if ($message) {
            $user_id = Yii::$app->user->id;
            Yii::error("$id\t$code\t$user_id\t$message", 'perfect');
        }
        Invoice::updateStatus($id, $code);
    }
}
