<?php

namespace frontend\controllers;

use common\models\Log;
use frontend\models\ChangePassword;
use Yii;
use common\models\User;
use common\models\UserSearch;
use yii\data\ActiveDataProvider;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * UserController implements the CRUD actions for User model.
 */
class UserController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],

            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index', 'view', 'update', 'matrix', 'change', 'history'],
                        'roles' => ['@']
                    ],
                    [
                        'allow' => true,
                        'actions' => ['create', 'delete'],
                        'roles' => ['admin']
                    ]
                ]
            ],

        ];
    }

    /**
     * Lists all User models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new UserSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single User model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id = null)
    {
        return $this->render('view', [
            'model' => $id ? $this->findModel($id) : Yii::$app->user->identity,
        ]);
    }

    /**
     * Creates a new User model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new User();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing User model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws ForbiddenHttpException
     * @throws NotFoundHttpException
     */
    public function actionUpdate($id)
    {
        if (!Yii::$app->user->can('manage') && Yii::$app->user->id != $id) {
            throw new ForbiddenHttpException('Вы можете редактировать только свой профиль');
        }

        $model = $this->findModel($id);
        if (Yii::$app->user->can('manage')) {
            $model->scenario = 'manage';
        }

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing User model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    public function actionMatrix($id = null) {
        $user = $id ? $this->findModel($id) : Yii::$app->user->identity;
        return $this->render('matrix', [
            'model' => $user
        ]);
    }

    public function actionChange() {
        $change = new ChangePassword();
        if ($change->load(Yii::$app->request->post()) && $change->validate()) {
            $user = Yii::$app->user->identity;
            if ($user->validatePassword($change->old_password)) {
                $user->setPassword($change->new_password);
//                $user->save();
                Yii::$app->db->createCommand('UPDATE user SET password_hash = :hash', [
                    ':hash' => $user->password_hash
                ])->execute();
                Yii::$app->session->setFlash('success', 'Пароль успешно изменен');
                return $this->redirect(['view']);
            }
            else {
                $change->addError('old_password', 'Неправильний пароль');
            }
        }

        return $this->render('change', [
            'model' => $change
        ]);
    }

    public function actionHistory($id = null) {
        $user = $id ? $this->findModel($id) : Yii::$app->user->identity;
        return $this->render('history', [
            'user' => $user,
            'dataProvider' => new ActiveDataProvider([
                'query' => Log::find()->where(['user_id' => $user->id]),
                'pagination' => [
                    'pageSize' => 100,
                ],
            ])
        ]);
    }

    /**
     * Finds the User model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return User the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = User::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
