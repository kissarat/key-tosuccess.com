<?php
/**
 * Created by PhpStorm.
 * User: taras
 * Date: 6/13/15
 * Time: 9:23 PM
 */

namespace console;


use yii\db\Migration;
use yii\helpers\ArrayHelper;

class CustomMigration extends Migration {

    /**
     * @param array $schema
     * @return array
     */
    public function timed(array $schema) {
        return ArrayHelper::merge([
            'time' => 'TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP',
            'ip' => 'INTEGER UNSIGNED'
        ], $schema);
    }

    /**
     * @inheritdoc
     */
    public function createTable($table, $columns, $options = '') {
        if ('mysql' == $this->db->driverName) {
            $options .= ' CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB ';
        }
        parent::createTable($table, $columns, $options);
    }
}
