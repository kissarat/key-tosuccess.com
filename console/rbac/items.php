<?php
return [
    'manage' => [
        'type' => 2,
    ],
    'admin' => [
        'type' => 1,
        'children' => [
            'manage',
        ],
    ],
];
