<?php
/**
 * Created by PhpStorm.
 * User: taras
 * Date: 6/14/15
 * Time: 10:56 PM
 */

namespace console\controllers;


use common\models\User;
use frontend\models\Tree;
use yii\console\Controller;

class HelloController extends Controller {
    public function actionIndex() {
        $users = User::find()->orderBy(['id' => SORT_ASC])->all();
        foreach($users as $user) {
            if ($user instanceof User && $user->tree instanceof Tree) {
                $members = $user->tree->members;
                $children = [];
                foreach($members as $member) {
                    $children[] = $member->user->username;
                }
                $members = implode(', ', $children);
                echo implode("\t", [$user->account, $user->tree->countMembers() ?: '', $user, $members]) . "\n";
            }
        }
    }
}