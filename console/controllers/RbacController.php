<?php
/**
 * Created by PhpStorm.
 * User: taras
 * Date: 6/8/15
 * Time: 4:48 AM
 */

namespace console\controllers;


use Yii;
use yii\console\Controller;

class RbacController extends Controller {

    public function actionInit() {
        $auth = Yii::$app->authManager;
        $can = $auth->createPermission('manage');
        $auth->add($can);
        $admin = $auth->createRole('admin');
        $auth->add($admin);
        $auth->addChild($admin, $can);
        $auth->assign($admin, 1);
    }
}
