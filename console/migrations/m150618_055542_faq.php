<?php

use console\CustomMigration;
use yii\db\Schema;

class m150618_055542_faq extends CustomMigration
{
    public function up()
    {
        $this->createTable('faq', [
            'id' => Schema::TYPE_PK,
            'question' => 'TINYTEXT NOT NULL',
            'answer' => 'TEXT NOT NULL',
        ]);
    }

    public function down()
    {
        $this->dropTable('faq');
    }
}
