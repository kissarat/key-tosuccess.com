CREATE VIEW matrix AS
  SELECT
    u.id, level, (stake * interest) as income
  FROM tree t
    JOIN user u ON t.user_id = u.id
    JOIN tree_type tt ON tt.id = t.type_id
    JOIN tree_level l ON l.type_id = tt.id;


CREATE VIEW payment AS
  SELECT u.*, sum(amount) as amount
  FROM user u JOIN invoice i ON u.id = i.sender_id
  GROUP BY sender_id;


CREATE VIEW qualification AS
  SELECT u.id, sum(amount) AS amount
  FROM user u JOIN invoice i ON u.id = i.sender_id
  WHERE from_unixtime(i.created_at) > DATE_SUB(CURDATE(), INTERVAL 1 MONTH)
        AND i.status = 'SUCCESS'
  GROUP BY sender_id;


CREATE VIEW children_count AS
  SELECT u.*, count(c.id) as count FROM user c JOIN user u ON c.parent_id = u.id
  GROUP BY u.id ORDER BY count DESC;


CREATE PROCEDURE transfer_from_invoices()
  BEGIN
    UPDATE user u
      JOIN invoice i ON i.sender_id = u.id
    SET u.account = u.account + i.amount;
  END;


CREATE PROCEDURE transfer(user_id INT, amount DECIMAL(8, 2))
  BEGIN
    UPDATE user u
    SET u.account = u.account + amount
    WHERE u.id = user_id;
  END;


CREATE PROCEDURE descend(uid INT) BEGIN
  DECLARE depth INT;
  SET depth = (SELECT count(*) FROM tree t
    JOIN tree_type tt ON tt.id = t.type_id
    JOIN tree_level l ON l.type_id = tt.id
  WHERE t.user_id = uid);

  IF depth > 0 THEN
    DROP TABLE IF EXISTS descendants;
    DROP TABLE IF EXISTS children;
    CREATE TEMPORARY TABLE descendants (
      id int,
      parent_id int,
      level int
    ) ENGINE = MEMORY;

    INSERT INTO descendants(id, level)
    VALUES (uid, 0);
    CREATE TEMPORARY TABLE children LIKE descendants;
#     ALTER TABLE descendants ADD CONSTRAINT UNIQUE(id);

    iter: LOOP
      INSERT INTO children
        SELECT u.id, u.parent_id, level + 1 FROM user u
          JOIN descendants h ON u.parent_id = h.id
        WHERE level < depth;
      DELETE FROM children WHERE id IN (SELECT id FROM descendants);
      IF (SELECT count(*) FROM children) > 0 THEN
        INSERT INTO descendants SELECT * FROM children;
      ELSE
        LEAVE iter;
      END IF;
    END LOOP;
    SELECT d.level, d.id, d.parent_id, u.created_at as time, u.username, u.email, u.skype FROM descendants d
      JOIN tree t ON d.id = t.user_id
      JOIN user u ON d.id = u.id;
#   DROP TABLE descendants;
#   DROP TABLE children;
  END IF;
END;


CREATE PROCEDURE enter(IN uid INT, IN number TINYINT) enter: BEGIN
  DECLARE pid INT;

#   IF (SELECT count(*) FROM tree WHERE user_id = uid) > 0 THEN
#     LEAVE enter;
#   END IF;

  IF 1 = number THEN
    UPDATE user u
#       JOIN tree t ON t.user_id = u.id
      JOIN tree_type tt ON tt.id = 1
    SET u.account = u.account - tt.stake
    WHERE u.id = uid;
    INSERT INTO tree (user_id) VALUES (uid);
  END IF;

  SET pid = (SELECT parent_id FROM user WHERE id = uid);

  UPDATE user u
    JOIN matrix m ON u.id = m.id
  SET u.account = u.account + income
  WHERE u.id = pid AND m.level = number;

  IF ROW_COUNT() > 0 AND pid IS NOT NULL THEN
    CALL enter(pid, number + 1);
  END IF;
END;


CREATE TABLE invoice_user (
  id INT NOT NULL,
  user_id INT NOT NULL,
  username TINYTEXT NOT NULL,
  amount DECIMAL(8, 2) NOT NULL,
  status TINYTEXT,
  wallet CHAR(8) NOT NULL,
  updated_at INT NOT NULL,
  ip INT UNSIGNED
);


DELIMITER $$
CREATE TRIGGER invoice_insert AFTER INSERT ON invoice FOR EACH ROW BEGIN
  IF 1 = NEW.sender_id THEN
    INSERT INTO invoice_user
    VALUES (NEW.id, NEW.receiver_id, (SELECT username FROM user WHERE id = NEW.receiver_id),
            - NEW.amount, NEW.status, NEW.payee, NEW.updated_at, NEW.ip);
  ELSE
    INSERT INTO invoice_user
    VALUES (NEW.id, NEW.sender_id, (SELECT username FROM user WHERE id = NEW.sender_id),
            NEW.amount, NEW.status, NEW.payer, NEW.updated_at, NEW.ip);
  END IF;
END;
$$
