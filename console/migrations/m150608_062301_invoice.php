<?php

use yii\db\Schema;
use yii\db\Migration;

class m150608_062301_invoice extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ('mysql' == $this->db->driverName) {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->addColumn('user', 'perfect', "CHAR(8) CHECK(perfect LIKE 'U???????')");
        $this->addColumn('user', 'account', 'DECIMAL(10,2) NOT NULL DEFAULT 0');

        $this->createTable('invoice', [
            'id' => Schema::TYPE_PK,
            'status' => 'TINYTEXT',
            'amount' => 'DECIMAL(8,2) NOT NULL CHECK(amount > 0)',
            'batch' => 'BIGINT UNSIGNED',
            'sender_id' => Schema::TYPE_INTEGER . ' NOT NULL REFERENCES user(id)',
            'receiver_id' => Schema::TYPE_INTEGER . ' NOT NULL REFERENCES user(id)',
            'payer' => "CHAR(8) NOT NULL CHECK(from_wallet LIKE 'U???????')",
            'payee' => "CHAR(8) NOT NULL CHECK(to_wallet LIKE 'U???????')",
            'memo' => 'TINYTEXT',
            'created_at' => Schema::TYPE_INTEGER . ' NOT NULL',
            'updated_at' => Schema::TYPE_INTEGER . ' NOT NULL',
            'ip' => 'INTEGER UNSIGNED'
        ], $tableOptions);
        $this->addForeignKey('invoice_sender', 'invoice', 'sender_id', 'user', 'id');
        $this->addForeignKey('invoice_receiver', 'invoice', 'receiver_id', 'user', 'id');
        $this->createIndex('invoice_id', 'invoice', 'id', true);
    }

    public function down()
    {
        $this->dropForeignKey('invoice_receiver', 'invoice');
        $this->dropForeignKey('invoice_sender', 'invoice');
        $this->dropIndex('invoice_id', 'invoice');
        $this->dropTable('invoice');
        $this->dropColumn('user', 'account');
        $this->dropColumn('user', 'perfect');
    }
}
