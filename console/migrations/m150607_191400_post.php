<?php

use yii\db\Schema;
use yii\db\Migration;

class m150607_191400_post extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ('mysql' == $this->db->driverName) {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('post', [
            'id' => Schema::TYPE_PK,
            'name' => Schema::TYPE_STRING,
            'title' => Schema::TYPE_STRING . ' NOT NULL',
            'content' => Schema::TYPE_TEXT . ' NOT NULL',
            'created_at' => Schema::TYPE_INTEGER . ' NOT NULL',
            'updated_at' => Schema::TYPE_INTEGER . ' NOT NULL',
            'ip' => 'INTEGER UNSIGNED'
        ], $tableOptions);
        $this->createIndex('post_id', 'post', 'id', true);
        $this->createIndex('post_name', 'post', 'name');

        $this->insert('post', [
            'name' => 'about',
            'title' => 'О нас',
            'content' => 'Описание',
            'created_at' => time(),
            'updated_at' => time(),
            'ip' => 0
        ]);
    }

    public function down()
    {
        $this->dropIndex('post_name', 'post');
        $this->dropIndex('post_id', 'post');
        $this->dropTable('post');
    }
}
