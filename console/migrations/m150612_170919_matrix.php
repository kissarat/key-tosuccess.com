<?php

use console\CustomMigration;
use yii\db\Schema;

class m150612_170919_matrix extends CustomMigration
{
    public static function load() {
        $sql = file_get_contents(__DIR__ . '/matrix.sql');
        return explode("\n\n\n", $sql);
    }

    public function up()
    {
        $this->createTable('tree_type', [
            'id' => Schema::TYPE_PK,
            'name' => 'TINYTEXT NOT NULL',
            'degree' => 'TINYINT UNSIGNED NOT NULL CHECK(degree > 0)',
            'priority' => 'TINYINT UNSIGNED NOT NULL',
            'stake' => 'DECIMAL(8,2) NOT NULL CHECK(stake > 0)',
        ]);

        $this->insert('tree_type', [
            'id' => 1,
            'name' => 'Матрица',
            'degree' => 3,
            'priority' => 1,
            'stake' => 30
        ]);

        $this->createTable('tree_level', [
            'id' => Schema::TYPE_PK,
            'type_id' => Schema::TYPE_INTEGER . ' NOT NULL',
            'level' => 'TINYINT UNSIGNED NOT NULL CHECK(level > 0)',
            'interest' => 'FLOAT(3,3) NOT NULL CHECK(interest > 0 AND interest < 1)',
        ]);
        $this->addForeignKey('tree_level', 'tree_level', 'type_id', 'tree_type', 'id');

        $levels = [
            1 => 0.5,
            2 => 0.015,
            3 => 0.015,
            4 => 0.015,
            5 => 0.25,
        ];
        foreach($levels as $level => $interest) {
            $this->insert('tree_level', [
                'type_id' => 1,
                'level' => $level,
                'interest' => $interest
            ]);
        }

        $this->createTable('tree', parent::timed([
            'id' => Schema::TYPE_PK,
            'user_id' => Schema::TYPE_INTEGER . ' NOT NULL UNIQUE',
            'type_id' => Schema::TYPE_INTEGER . ' NOT NULL DEFAULT 1',
        ]));
        $this->addForeignKey('tree_type', 'tree', 'type_id', 'tree_type', 'id');
        $this->addForeignKey('tree_user', 'tree', 'user_id', 'user', 'id');
        $this->createIndex('tree_id', 'tree', 'id', true);

        foreach(static::load() as $query) {
            $this->execute($query);
        }
    }

    public function down()
    {
        $queries = static::load();
        $queries = array_reverse($queries);
        foreach($queries as $query) {
            if (preg_match('|^CREATE ([A-Z]+ [a-z_]+)|', $query, $match)) {
                $this->execute('DROP ' . $match[1]);
            }
        }
        $this->dropIndex('tree_id', 'tree');
        $this->dropForeignKey('tree_user', 'tree');
        $this->dropForeignKey('tree_type', 'tree');
        $this->dropTable('tree');

        $this->dropForeignKey('tree_level', 'tree_level');
        $this->dropTable('tree_level');

        $this->dropTable('tree_type');
    }
}
