<?php

use console\CustomMigration;
use yii\db\Schema;
use yii\db\Migration;

class m150610_094605_profile extends CustomMigration
{
    public function up()
    {
        $this->addColumn('user', 'first', 'TINYTEXT');
        $this->addColumn('user', 'last', 'TINYTEXT');
        $this->addColumn('user', 'phone', 'TINYTEXT');
        $this->addColumn('user', 'skype', 'TINYTEXT');
        $this->addColumn('user', 'parent_id', 'INT REFERENCES user(id)');
        $this->addForeignKey('parent', 'user', 'parent_id', 'user', 'id');

        $this->createTable('log', parent::timed([
            'id' => Schema::TYPE_PK,
            'user_id' => 'INT NOT NULL REFERENCES user(id)',
            'type' => 'TINYINT NOT NULL'
        ]));
        $this->addForeignKey('log_user', 'log', 'user_id', 'user', 'id');
        $this->createIndex('log_id', 'log', 'id', true);
    }

    public function down()
    {
        $this->dropColumn('user', 'first');
        $this->dropColumn('user', 'last');
        $this->dropColumn('user', 'phone');
        $this->dropColumn('user', 'skype');

        $this->dropForeignKey('parent', 'user');
        $this->dropColumn('user', 'parent_id');

        $this->dropIndex('log_id', 'log');
        $this->dropTable('log');
    }
}
